import React from "react";
import {StyledEngineProvider} from '@mui/material/styles';
import {Provider} from 'react-redux';
import store from './store';
import routes from './configs/routesConfig';
import AppContext from "./AppContext";
import {BrowserRouter} from "react-router-dom";

const withAppProviders = (Component: React.ComponentType) => {
    return () => (
        <AppContext.Provider
            value={{
                routes,
            }}
        >
            <BrowserRouter>
                <Provider store={store}>
                    <StyledEngineProvider injectFirst>
                        <Component/>
                    </StyledEngineProvider>
                </Provider>
            </BrowserRouter>
        </AppContext.Provider>
    );
};

export default withAppProviders;
