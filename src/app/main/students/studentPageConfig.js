import StudentPage from './StudentPage';

const StudentPageConfig = {
  routes: [
    {
      path: 'students',
      element: <StudentPage />,
    },
  ],
};

export default StudentPageConfig;
