import {useMutation, useQueryClient} from "react-query";
import {createStudent, deleteStudent, updateStudent} from "./services";
import _ from "../../../@lodash";
import {closeDialog} from "../../store/dialogSlice";
import {useSnackbar} from "notistack";
import {useDispatch} from "react-redux";

function useCRUD() {
    const queryClient = useQueryClient();
    const {enqueueSnackbar} = useSnackbar();
    const dispatch = useDispatch();

    const deleteStudentMutation = useMutation(deleteStudent, {
        onSuccess: (data, variables) => {
            queryClient.setQueryData(["getStudents"], prevData => {
                // @ts-ignore
                return _.remove(prevData, function (item) {
                    // @ts-ignore
                    return item.id !== variables
                });
            })
            enqueueSnackbar("Student Deleted", {
                variant: 'success',
            });
            dispatch(closeDialog())
        }
    })

    const createStudentMutation = useMutation(createStudent, {
        onSuccess: data => {
            queryClient.setQueryData(["getStudents"], prevData => {
                // @ts-ignore
                return [data, ...prevData]
            })
            enqueueSnackbar("Student Created", {
                variant: 'success',
            });
            dispatch(closeDialog())
        }
    });

    const editStudentMutation = useMutation(updateStudent, {
        onSuccess: (data, variables) => {
            queryClient.setQueryData(["getStudents"], prevData => {
                // @ts-ignore
                return _.map(prevData, function (item) {
                    // @ts-ignore
                    return item.id === variables.id ? data : item;
                });
            })
            enqueueSnackbar("Student Updated", {
                variant: 'success',
            });
            dispatch(closeDialog())
        }
    });

    return {createStudentMutation, deleteStudentMutation, editStudentMutation}
}


const studentsHook = {
    useCRUD
}
export default studentsHook;