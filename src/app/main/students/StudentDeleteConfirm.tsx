import Button from '@mui/material/Button';
import studentHooks from "./studentHooks";
import ConfirmDialog from "../../shared-components/DeleteConfirmDialog";

// @ts-ignore
function StudentDeleteConfirm({title = "", detail = "", id}) {
    const {deleteStudentMutation} = studentHooks.useCRUD();

    return (
        <ConfirmDialog title={title} detail={detail}>
            <Button
                variant="contained"
                color="error"
                className=" w-full mt-16"
                aria-label="Sign in"
                onClick={() => deleteStudentMutation.mutate(id)}
                type="button"
                size="large"
                data-role="button-form"
                disabled={deleteStudentMutation.isLoading}
            >
                Confirm
            </Button>

        </ConfirmDialog>
    );
}

export default StudentDeleteConfirm;
