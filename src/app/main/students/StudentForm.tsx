// @ts-nocheck
import Button from '@mui/material/Button';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import Form from "../../shared-components/form/Form";
import Input from "../../shared-components/form/Input";
import {DialogContent, DialogTitle} from "@mui/material";
import {closeDialog} from "../../store/dialogSlice";
import {useDispatch} from "react-redux";
import studentHooks from "./studentHooks";

const schema = yup.object().shape({
    dni: yup.string().required('You must enter a DNI'),
    name: yup
        .string()
        .required('Please enter your Name.')
        .max(20, 'Name is too big - must be less than 20 characters.'),
    lastName: yup
        .string()
        .required('Please enter your Last name.')
        .max(20, 'Name is too big - must be less than 20 characters.'),
    age: yup
        .number()
        .required('Please enter your Age.')
        .min(18, 'Age is a negative - must be under 18 years old.')
        .max(80, 'Age is too big - must be less than 80 years.'),
});
const initialValues = {
    dni: '',
    name: '',
    lastName: '',
    age: 18
};

const defaultValues = {
    mode: 'onChange',
    defaultValues: initialValues,
    resolver: yupResolver(schema),
};

function StudentForm({errors = null, student}) {
    const {createStudentMutation, editStudentMutation} = studentHooks.useCRUD();
    const dispatch = useDispatch();
    const isNew = student === "new";
    const titleLabel = isNew ? "New" : "Edit";

    function onSubmit(data) {
        if (isNew)
            createStudentMutation.mutate(data)
        else
            editStudentMutation.mutate(data)
    }

    return (
        <>
            <DialogTitle id="alert-dialog-title">{`${titleLabel} Student`} </DialogTitle>
            <DialogContent>
                <Form
                    name="studentForm"
                    noValidate
                    className="flex flex-col justify-center w-full sm:min-w-xs mt-32"
                    onSubmit={onSubmit}
                    defaultValues={defaultValues}
                    data={student}
                    autoComplete={"off"}
                    _errors={errors}
                >
                    <Input.Text name="dni" label="DNI" type="text" required autoFocus/>

                    <Input.Text name="name" label="Name" type="text" required/>

                    <Input.Text name="lastName" label="Last name" type="text" required/>

                    <Input.Text name="age" label="Age" type="number" required/>

                    <div className="flex flex-row  space-x-10 justify-center w-full sm:min-w-xs"
                         data-role="input-container">
                        <Button
                            onClick={() => dispatch(closeDialog())}
                            variant="contained"
                            className=" w-full mt-16"
                            aria-label="Cancel"
                            size="large"
                            data-role="button-form"
                            disabled={false}
                        >
                            Cancel
                        </Button>
                        <Button
                            variant="contained"
                            color="secondary"
                            className=" w-full mt-16"
                            aria-label="Save"
                            type="submit"
                            size="large"
                            data-role="button-form"
                            isLoading={createStudentMutation.isLoading || editStudentMutation.isLoading}
                        >
                            Save
                        </Button>
                    </div>
                </Form>
            </DialogContent>
        </>
    );
}

export default StudentForm;
