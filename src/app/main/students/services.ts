import axios from 'axios';
import {Student} from "./Student";

const STUDENT_ENDPOINT = "/v2/students"

export function getStudents() {
    return axios.get(STUDENT_ENDPOINT).then((resp) => resp.data);
}

export function createStudent(data: Student) {
    return axios.post(STUDENT_ENDPOINT, data).then((resp) => resp.data);
}

export function deleteStudent(id: string | number) {
    return axios.delete(STUDENT_ENDPOINT + "/" + id).then((resp) => resp.data);
}

export function updateStudent(data: Student) {
    return axios.put(STUDENT_ENDPOINT + "/" + data.id, data).then((resp) => resp.data);

}
