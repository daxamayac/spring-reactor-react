export interface Student {
    id: number | string
    dni: string
    name: string
    lastName: string
    age: number
}
