import EnrollmentPage from "./EnrollmentPage";


const EnrollmentPageConfig = {
    routes: [
        {
            path: 'enrollments',
            element: <EnrollmentPage/>,
        },
    ],
};

export default EnrollmentPageConfig;
