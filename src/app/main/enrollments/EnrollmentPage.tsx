// @ts-nocheck
import Typography from '@mui/material/Typography';
import Button from "@mui/material/Button";
import {useQuery} from "react-query";
import {Autocomplete, CircularProgress} from "@mui/material";
import {useEffect, useState} from "react";
import {getCourses} from "../courses/services";
import TextField from "@mui/material/TextField";
import {getStudents} from "../students/services";
import {Controller, useForm} from "react-hook-form";
import {yupResolver} from "@hookform/resolvers/yup/dist/yup";
import * as yup from "yup";
import enrollmentHooks from "./enrollmentHooks";
import {Course} from "../courses/Course";
import _ from "../../../@lodash";
import {getEnrollmentsByStudent} from "./services";

const param = {status: true}

const schema = yup.object().shape({
    courseSelected: yup.object().required('You must select a course.')
        .typeError('You must select a course.'),
    studentSelected: yup.object().required('You must select a student.')
        .typeError('You must select a student.')
});
const initialValues = {
    courseSelected: null,
    studentSelected: null,
}
const initValueEnrollment = {
    id: null,
    student: {},
    courses: {}
}
const defaultValues = {
    mode: 'onChange',
    defaultValues: initialValues,
    resolver: yupResolver(schema),
};

function EnrollmentPage() {
    const {control, formState, handleSubmit, setValue, getValues, reset, watch} = useForm({...defaultValues});
    const {errors} = formState;
    const {createEnrollmentMutation, editEnrollmentMutation} = enrollmentHooks.useCRUD();
    const [coursesEnrolled, setCoursesEnrolled] = useState([]);
    const [isValid, setIsValid] = useState(false);
    const watchStudentSelected = watch("studentSelected", false);
    const [currentEnrollment, setCurrentEnrollment] = useState(initValueEnrollment)

    const {
        isFetching: isFetchingEnrollments,
        refetch
    } = useQuery(["getEnrollmentsByStudent"], () => getEnrollmentsByStudent({id: getValues("studentSelected")?.id}), {
        retry: false,
        refetchOnWindowFocus: false,
        enabled: false,

    });

    const {
        isLoading: loadingCourses,
        data: courses
    } = useQuery(["getCourses", param], () => getCourses(param), {retry: false, refetchOnWindowFocus: false})

    const {isLoading: loadingStudents, data: students} = useQuery("getStudents", getStudents, {
        retry: false,
        refetchOnWindowFocus: false
    })

    useEffect(() => {
            if (watchStudentSelected)
                refetch().then((resp) => {
                    if (resp.status === "success") {

                        const data = resp.data;
                        setCurrentEnrollment(data);
                        setCoursesEnrolled(data.courses)
                    } else {
                        setCoursesEnrolled([])
                        setCurrentEnrollment(initValueEnrollment)
                    }
                })
        }, [refetch, watchStudentSelected]
    )

    function onAddCourse(data) {
        addCourseEnrolled(data.courseSelected)
    }

    function addCourseEnrolled(course: Course) {
        setCoursesEnrolled((oldCoursesEnrolled => [...oldCoursesEnrolled, course]))
        setValue("courseSelected", null)
    }

    function removeCourseEnrolled(id: string | number) {
        setCoursesEnrolled((oldCoursesEnrolled => oldCoursesEnrolled.filter((item) => item.id !== id)))
    }

    useEffect(() => {
            if (_.isEmpty(coursesEnrolled)) {
                setIsValid(false)
            } else {
                setIsValid(true)
            }

        }, [coursesEnrolled]
    )

    function onEnroll() {

        if (currentEnrollment.id === null) {
            const payload = {
                id: null,
                student: {id: getValues("studentSelected").id},
                courses: coursesEnrolled
            }

            createEnrollmentMutation.mutateAsync(payload).then(() => {
                    reset(initialValues)
                    setCoursesEnrolled([])
                }
            )
        } else {
            const payload = {
                id: currentEnrollment.id,
                student: currentEnrollment.student,
                enrollmentDate: new Date(),
                courses: coursesEnrolled,
            }
            editEnrollmentMutation.mutateAsync(payload).then(() => {
                reset(initialValues)
                setCoursesEnrolled([])
            })
        }
    }

    return (
        <div className="flex flex-col flex-auto items-center mt-48 min-w-0 md:p-32">
            <Typography variant="h3">Enrollments</Typography>
            <div className="w-8/12 ">
                <form onSubmit={handleSubmit(onAddCourse)}>
                    <div className="flex items-center mb-10">
                        <Controller
                            name="studentSelected"
                            control={control}
                            defaultValue={[]}
                            render={({field: {onChange, value, onBlur, ref}}) => (
                                <Autocomplete
                                    className="w-full sm:w-8/12 "
                                    options={students || []}
                                    value={value}
                                    openOnFocus
                                    onChange={(event, newValue) => {
                                        setValue("courseSelected", null)
                                        onChange(newValue);
                                    }}

                                    isOptionEqualToValue={(option, value) => option.id === value.id}
                                    getOptionLabel={(option) => option.name + " " + option.lastName}
                                    renderInput={(params) => <TextField
                                        {...params}
                                        label="Students"
                                        InputProps={{
                                            ...params.InputProps,
                                            endAdornment: (
                                                <>
                                                    {loadingStudents ?
                                                        <CircularProgress color="inherit" size={20}/> : null}
                                                    {params.InputProps.endAdornment}
                                                </>
                                            ),
                                        }}
                                        error={!!errors.studentSelected}
                                        helperText={errors?.studentSelected?.message}
                                        onBlur={onBlur}
                                        inputRef={ref}
                                    />}
                                    loading={loadingStudents}
                                    loadingText={"Loading.."}
                                    noOptionsText={"No data options"}
                                />
                            )}
                        />

                    </div>

                    <div className="flex flex-col sm:flex-row justify-between items-center mb-10">
                        <Controller
                            name="courseSelected"
                            control={control}
                            defaultValue={[]}
                            render={({field: {onChange, value, onBlur, ref}}) => (

                                <Autocomplete
                                    className="w-full sm:w-8/12 "
                                    options={_.differenceBy(courses, coursesEnrolled, value => value.id)}
                                    value={value}
                                    openOnFocus
                                    onChange={(event, newValue) => {
                                        onChange(newValue);
                                    }}
                                    isOptionEqualToValue={(option, value) => option.id === value.id}
                                    getOptionLabel={(option) => option?.name ?? ""}
                                    renderInput={(params) => <TextField
                                        {...params}
                                        label="Course"
                                        InputProps={{
                                            ...params.InputProps,
                                            endAdornment: (
                                                <>
                                                    {loadingCourses ?
                                                        <CircularProgress color="inherit" size={20}/> : null}
                                                    {params.InputProps.endAdornment}
                                                </>
                                            ),
                                        }}
                                        error={!!errors.courseSelected}
                                        helperText={errors?.courseSelected?.message}
                                        onBlur={onBlur}
                                        inputRef={ref}
                                    />}
                                    loading={loadingCourses}
                                    loadingText={"Loading.."}
                                    noOptionsText={"No data options"}
                                />
                            )}
                        />

                        <Button
                            variant="contained"
                            color="primary"
                            className="w-full sm:w-fit  mt-16 "
                            aria-label="Sign in"
                            size="large"
                            type="submit"
                            data-role="button-form"
                        >
                            Add course
                        </Button>
                    </div>
                </form>
                <div>
                    <br/>
                    <Typography variant="h5">
                        Lista de cursos
                        de: <strong> {getValues("studentSelected") && getValues("studentSelected").name + getValues("studentSelected").lastName} </strong>
                    </Typography>
                    <div className="mt-48">
                        {isFetchingEnrollments && <CircularProgress/>}
                        {!isFetchingEnrollments && _.isEmpty(coursesEnrolled) && <strong>No data</strong>}
                        {coursesEnrolled && coursesEnrolled.map((item) => (
                            <div key={item.id}>{item.name}

                                <Button
                                    onClick={() => removeCourseEnrolled(item.id)}
                                    color="error"
                                >
                                    Remover
                                </Button>
                            </div>
                        ))}
                    </div>
                </div>
                <div className="w-full flex justify-end">
                    <Button
                        variant="contained"
                        color="primary"
                        className="w-full sm:w-fit  mt-16 "
                        aria-label="Sign in"
                        size="large"
                        type="button"
                        onClick={onEnroll}
                        disabled={!isValid}
                        data-role="button-form"
                    >
                        Enroll
                    </Button>
                </div>
            </div>
        </div>
    );
}

export default EnrollmentPage;
