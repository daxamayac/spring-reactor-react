import CoursePage from './CoursePage';

const CoursePageConfig = {
    routes: [
        {
            path: 'courses',
            element: <CoursePage/>,
        },
    ],
};

export default CoursePageConfig;
