import SignOutPage from './SignOutPage';

const SignOutPageConfig = {
  routes: [
    {
      path: 'sign-out',
      element: <SignOutPage />,
    },
  ],
};

export default SignOutPageConfig;
