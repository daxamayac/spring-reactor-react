import {useEffect} from 'react';
import {signOut} from "../../auth/authServices";
import {useNavigate} from "react-router-dom";

function SignOutPage() {
    const navigate = useNavigate();
    useEffect(() => {
            signOut();
            navigate("sign-in");
        }
    )

    return (
        <></>
    );
}

export default SignOutPage;
