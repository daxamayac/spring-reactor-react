import Error404Page from './Error404Page';

const Error404Config = {
  routes: [
    {
      path: '404',
      element: <Error404Page />,
    },
  ],
};

export default Error404Config;
