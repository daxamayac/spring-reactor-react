import Box from '@mui/material/Box';

function SignInWelcome() {
    return (
        <Box
            className="relative hidden md:flex flex-auto items-center justify-center h-full p-64 lg:px-112 overflow-hidden"
            sx={{backgroundColor: 'primary.main'}}
        >

            <div className="z-10 relative w-full max-w-2xl">
                <div className="text-7xl font-bold leading-none text-gray-100">
                    <div>Spring WebFlux & React</div>
                </div>
                <div className="mt-24 text-lg tracking-tight leading-6 text-gray-400">
                    This is the last project of course.
                </div>
                <div className="flex items-center mt-32">

                    <div className=" font-medium tracking-tight text-gray-400">
                        By @daxamayac
                    </div>
                </div>
            </div>
        </Box>
    );
}

export default SignInWelcome;
