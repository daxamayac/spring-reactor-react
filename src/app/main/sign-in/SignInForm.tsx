// @ts-nocheck
import Button from '@mui/material/Button';
import * as yup from 'yup';
import {yupResolver} from '@hookform/resolvers/yup';
import Form from "../../shared-components/form/Form";
import Input from "../../shared-components/form/Input";

/**
 * Form Validation Schema
 */
const schema = yup.object().shape({
  email: yup.string().email('You must enter a valid email').required('You must enter a email'),
  password: yup
    .string()
    .required('Please enter your password.')
    .min(4, 'Password is too short - must be at least 4 chars.'),
});

const defaultValues = {
  mode: 'onChange',
  defaultValues: {
    email: 'admin@admin.com',
    password: '',
    remember: true,
  },
  resolver: yupResolver(schema),
};

function SignInForm({className, onSubmit, errors , isLoading}) {
  // @ts-ignore
  return (
    <Form
      name="loginForm"
      noValidate
      className={className}
      onSubmit={onSubmit}
      defaultValues={defaultValues}
      _errors={errors}
    >
      <Input.Text name="email" label="Email" type="email" required autoFocus />

      <Input.Text name="password" label="Password" type="password" required />

      <Button
        variant="contained"
        color="secondary"
        className=" w-full mt-16"
        aria-label="Sign in"
        type="submit"
        size="large"
        data-role="button-form"
        isLoading={isLoading}
      >
        Sign in
      </Button>

    </Form>
  );
}

export default SignInForm;
