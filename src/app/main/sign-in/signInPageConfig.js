import SignInPage from './SignInPage';

const SignInPageConfig = {
  routes: [
    {
      path: 'sign-in',
      element: <SignInPage />,
    },
  ],
};

export default SignInPageConfig;
