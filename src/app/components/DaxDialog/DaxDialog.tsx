import Dialog from '@mui/material/Dialog';
import { useDispatch, useSelector } from 'react-redux';
import {closeDialog, selectDialogOptions, selectDialogState} from "../../store/dialogSlice";


function DaxDialog() {
  const dispatch = useDispatch();
  const state = useSelector(selectDialogState);
  const options = useSelector(selectDialogOptions);
const {children, ...props} = options;
  return (
    <Dialog
      open={state}
      onClose={(ev) => dispatch(closeDialog())}
      aria-labelledby="dialog-title"
      classes={{
        paper: 'rounded-8',
      }}
      {...props}
    >
        {children}
    </Dialog>
  );
}

export default DaxDialog;
