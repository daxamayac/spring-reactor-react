import TextField from '@mui/material/TextField';
import FormControl from '@mui/material/FormControl';
import FormControlLabel from '@mui/material/FormControlLabel';
import Checkbox from '@mui/material/Checkbox';
import FormHelperText from '@mui/material/FormHelperText';

// @ts-ignore
function Text({ name, label, field, errors, type, ...rest }) {
  return (
    <TextField
      {...field}
      className="mb-24"
      label={label}
      type={type}
      error={!!errors}
      helperText={errors?.message}
      variant="outlined"
      fullWidth
      {...rest}
    />
  );
}

// @ts-ignore
function Check({ name, label, field, errors, ...rest }) {
  return (
    <FormControl className="items-center" error={!!errors}>
      <FormControlLabel
        label={label}
        control={<Checkbox checked={field.value} {...field} size="small" {...rest} />}
      />
      <FormHelperText>{errors?.message}</FormHelperText>
    </FormControl>
  );
}

const Input = {
  Text,
  Check,
};
export default Input;
