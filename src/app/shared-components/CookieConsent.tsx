import {CardHeader, Link} from '@mui/material';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import Fab from '@mui/material/Fab';
import Typography from '@mui/material/Typography';
import {motion} from 'framer-motion';
import {memo, useEffect, useState} from 'react';

function CookieConsent({ onAccept = () => {} }) {
  const [cookieConsent, setCookieConsent] = useState(true);

  useEffect(() => {
    window.addEventListener('focus', onFocus);
    onFocus();
    return function cleanup() {
      window.removeEventListener('focus', onFocus);
    };
  }, []);

  function handleAccept() {
    localStorage.setItem('cookieConsent', 'true');
    setCookieConsent(true);
    onAccept();
  }

  const onFocus = () => {
    if (!localStorage.getItem('cookieConsent')) {
      localStorage.removeItem('cookieConsent');
      setCookieConsent(false);
    }
  };

  return (
    <>
      {!cookieConsent && (
        <div className="w-full fixed left-0 bottom-0 z-99">
          <motion.div
            initial={{ opacity: 0, scale: 0.6 }}
            animate={{ opacity: 1, scale: 1 }}
            transition={{ delay: 0.1 }}
          >
            <Card className="z-9999 rounded-none p-24" variant="outlined">
              <CardHeader title="¿Aceptar cookies?" />
              <CardContent className="flex items-center justify-center p-16 sm:p-24 md:p-32">
                <Typography className="flex-auto text-justify px-24 sm:px-32">
                  Usamos cookies propias y de terceros que nos sirven para recordar vuestras
                  preferencias y analizar el uso que usted da al sitio. Para continuar navegando en
                  el sitio debes consentir el uso de cookies. Obtén más información sobre nuestra{' '}
                  <Link href="/" underline="hover" target="_blank">
                    Política de cookies
                  </Link>
                  .
                </Typography>
                <div className="flex-1">
                  <Fab
                    color="secondary"
                    variant="extended"
                    size="small"
                    onClick={() => handleAccept()}
                  >
                    Aceptar
                  </Fab>
                </div>
              </CardContent>
            </Card>
          </motion.div>
          <div className="fixed left-0 top-0 w-full h-full -z-10 bg-black bg-opacity-80" />
        </div>
      )}
    </>
  );
}

export default memo(CookieConsent);
