import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import {memo} from 'react';
import clsx from 'clsx';

interface footerProps {
    className?: string
}

function Footer({className}: footerProps) {

    return (
        <AppBar
            id="footer"
            className={clsx('relative z-20 shadow-md', className)}
            color="default"
        >
            <Toolbar className="min-h-48 md:min-h-64 px-8 sm:px-12 py-0 flex items-center overflow-x-auto">
                Copyright © @daxstack 2022 by @daxamayac
            </Toolbar>
        </AppBar>
    );
}

export default memo(Footer);
