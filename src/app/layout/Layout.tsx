import React, {useContext} from "react";
import {useLocation, useRoutes} from "react-router-dom";
import AppContext from "../AppContext";
import Footer from "./Footer";
import Toolbar from "./Toolbar";
import DaxDialog from "../components/DaxDialog";

interface LayoutProps {
    children?: | React.ReactElement | React.ReactElement[];
}

function Layout({children}: LayoutProps) {
    const appContext = useContext(AppContext);
    // @ts-ignore
    const {routes} = appContext;
    const location = useLocation();
    const isSignIn = location.pathname === "/sign-in" || location.pathname === "/404";

    return <div id="layout" className="w-full flex">
        <DaxDialog />
        <div className="flex flex-auto min-w-0">
            <main id="main" className="flex flex-col flex-auto min-h-full min-w-0 relative z-10">
                {!isSignIn && <Toolbar/>}

                <div className="flex flex-col flex-auto min-h-0 relative z-10">

                    {useRoutes(routes)}
                    {children}
                </div>

                {!isSignIn && <Footer/>}
            </main>

        </div>

    </div>
}

export default Layout;