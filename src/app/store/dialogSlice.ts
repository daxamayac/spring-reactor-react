import {createSelector, createSlice} from '@reduxjs/toolkit';

const dialogSlice = createSlice({
  name: 'dialog',
  initialState: {
    state: false,
    options: {
      children: 'children'
    },
  },
  reducers: {
    openDialog: (state, action) => {
      state.state = true;
      state.options = action.payload;
    },
    closeDialog: (state) => {
      state.state = false;
    },
  },
});

export const { openDialog, closeDialog } = dialogSlice.actions;

export const dialogStateSelector = (state: any) => state.dialog;

export const selectDialogState = createSelector(
    dialogStateSelector,
    (dialogState) => {
      return dialogState.state;
    }
);

export const selectDialogOptions = createSelector(
    dialogStateSelector,
    (dialogState) => {
      return dialogState.options;
    }
);

export default dialogSlice.reducer;
