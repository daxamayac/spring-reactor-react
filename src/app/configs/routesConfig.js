import enrollmentPageConfig from "../main/enrollments/enrollmentPageConfig";
import coursePageConfig from "../main/courses/coursePageConfig";
import studentPageConfig from "../main/students/studentPageConfig";
import signOutPageConfig from "../main/sign-out/signOutPageConfig";
import Error404Page from "../main/404/Error404Page";
import error404Config from "../main/404/error404Config";
import signInPageConfig from "../main/sign-in/signInPageConfig";

import {Navigate} from 'react-router-dom';

const routeConfigs = [
    signInPageConfig,
    signOutPageConfig,
    studentPageConfig,
    coursePageConfig,
    enrollmentPageConfig,
    error404Config
];

function generateRoutesFromConfigs(configs) {
    let allRoutes = [];
    configs.forEach((config) => {
        allRoutes = [...allRoutes, ...setRoutes(config)];
    });
    return allRoutes;
}

function setRoutes(config) {
    let _routes = [...config.routes];
    return [..._routes];
}

const routesConfig = [
    ...generateRoutesFromConfigs(routeConfigs),
    {
        path: '/',
        element: <Navigate to="sign-in"/>
    },
    {
        path: '404',
        element: <Error404Page/>
    },
    {
        path: '*',
        element: <Navigate to="404"/>,
    },
];

export default routesConfig;
