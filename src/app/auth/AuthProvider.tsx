import React, {memo} from "react";
import {Navigate, useLocation} from 'react-router-dom';
import {getAccessToken, isAuthTokenValid, setSession} from "./authServices";

interface authProviderProps {
    children?: | React.ReactElement | React.ReactElement[];
}

function AutProvider({children}: authProviderProps) {
    const location = useLocation();
    const access_token = getAccessToken();

    if (access_token && isAuthTokenValid(access_token)) {
        setSession(access_token)
        return <>{children}</>;
    } else if (location.pathname === "/sign-in") {
        return <>{children}</>;
    }
    return <Navigate to={"sign-in"}/>;

};

export default memo(AutProvider);
