import React from 'react';
import withAppProviders from "./withAppProviders";
import CookieConsent from "./shared-components/CookieConsent";
import {createTheme, ThemeProvider} from "@mui/material";
import {themeOptions} from "./configs/themeConfig";
import Layout from "./layout/Layout";
import AuthProvider from "./auth/AuthProvider";
import {QueryClient, QueryClientProvider} from 'react-query'
import axios from "axios";
import {SnackbarProvider} from "notistack";

axios.defaults.baseURL = process.env.REACT_APP_API_BASEPATH;

axios.interceptors.response.use(function (response) {
    return response;
}, function (error) {
    if (error.response.status === 403 || error.response.status === 401 || error.response.status === 0) {
        if (window.location.pathname !== "/sign-in")
            window.location.href = '/sign-in'
    }
    return Promise.reject(error)
})

function App() {
    const queryClient = new QueryClient();

    return (
        <ThemeProvider theme={createTheme(themeOptions)}>
            <AuthProvider>
                <QueryClientProvider client={queryClient}>
                    <SnackbarProvider
                        maxSnack={3}
                        autoHideDuration={3000}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        classes={{
                            containerRoot: 'bottom-0 right-0 mb-52 md:mb-68 mr-8 lg:mr-80 z-99',
                        }}
                    >
                        <Layout>
                            <CookieConsent onAccept={() => console.log("Thanks by agree. Att: @daxamayac")}/>
                        </Layout>
                    </SnackbarProvider>
                </QueryClientProvider>
            </AuthProvider>
        </ThemeProvider>
    );
}

export default withAppProviders(App);
