const {SubresourceIntegrityPlugin} = require( "webpack-subresource-integrity");

module.exports = function override(config, env) {
    config.output.crossOriginLoading = 'use-credentials'
    config.plugins.push(
        new SubresourceIntegrityPlugin({
            hashFuncNames: ['sha256', 'sha384'],
            enabled: true
        })
    )
    return config
}